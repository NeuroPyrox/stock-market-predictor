import enum
import inspect
import schema
import test

test.monitor_inputs()

class Quote:
    
    @singleton
    class Keys:
            

Test()

def test_chart_schema():
    chart = [{
            market_high:                1,
            market_low:                 2,
            market_open:                3,
            market_close:               4,
            market_average:             5,
            market_volume:              6,
            market_number_of_trades:    7,
            'extra_key':                0,
        },{
            market_high:                -1,
            market_low:                 -1,
            market_open:                10,
            market_close:               11,
            market_average:             -1,
            market_volume:              0,
            market_number_of_trades:    14,
        }]
    chart = chart_schema.validate(chart)
    expected_chart = [{
            market_high:                1,
            market_low:                 2,
            market_open:                3,
            market_close:               4,
            market_average:             5,
            market_volume:              6,
            market_number_of_trades:    7,
        },None]
    assert_equal(chart, expected_chart)

test_functions.append(test_chart_schema)

def chart_url(symbol, date):
    return 'https://api.iextrading.com/1.0/stock/{}/chart/date/{}'.format(
        symbol, date.strftime('%Y%m%d'))

def request_chart(symbol, date):
    url = chart_url(symbol, date)
    return requests.get(url).json()

day = datetime.timedelta(days=1)
minutes_per_trading_day = 390
chart_delay_minutes = 15

def assert_equal(a, b):
    assert a == b, '{} != {}'.format(a, b)

def validate_full_day_chart(chart):
    chart = chart_schema.validate(chart)
    if len(chart) == 0:
        return False
    else:
        assert_equal(len(chart), minutes_per_trading_day)
        return True

test_symbols = ['sqqq', 'f', 'aapl', 'spy']

def test_request_chart_past():
    today = datetime.date.today()
    at_least_one_day_open = False
    for days_ago in range(1, 5):
        date = today - (days_ago * day)
        stock_market_was_open = None
        for symbol in test_symbols:
            chart = request_chart(symbol, date)
            if stock_market_was_open == None:
                stock_market_was_open = validate_full_day_chart(chart)
            else:
                assert_equal(validate_full_day_chart(chart),
                             stock_market_was_open)
        if stock_market_was_open:
            at_least_one_day_open = True
    assert at_least_one_day_open

test_functions.append(test_request_chart_past)

def chart_to_matrix(chart):
    # Assumes none of the rows are empty
    matrix = np.empty((len(chart), quote_size))
    for row, quote in enumerate(chart):
        for key, value in quote.items():
            column = quote_indices[key]
            matrix[row, column] = value
    return matrix

def test_chart_to_matrix():
    matrix = np.array([[1, 2, 3, 4, 5, 6, 7], [8, 9, 10, 11, 12, 13, 14]])
    chart = [{
            market_high:                1,
            market_low:                 2,
            market_open:                3,
            market_close:               4,
            market_average:             5,
            market_volume:              6,
            market_number_of_trades:    7,
        },{
            market_high:                8,
            market_low:                 9,
            market_open:                10,
            market_close:               11,
            market_average:             12,
            market_volume:              13,
            market_number_of_trades:    14,
        }]
    assert (chart_to_matrix(chart) == matrix).all()

test_functions.append(test_chart_to_matrix)

def generate_day_progress(chart):
    progress = []
    for minute in range(0, minutes_per_trading_day):
        if len(chart) <= minute:
            break
        if chart[minute] == None:
            continue
        progress.append(minute)
    return np.array(progress) / (minutes_per_trading_day - 1)

epsilon = 0.000001

def get_chart_matrix(symbol, date):
    chart = request_chart(symbol, date)
    chart = chart_schema.validate(chart)
    day_progress = generate_day_progress(chart)
    chart = [quote for quote in chart if quote != None]
    matrix = chart_to_matrix(chart)
    matrix = np.log(matrix + epsilon)
    return np.concatenate((matrix, day_progress[:, np.newaxis]), axis=1)

# Chart is on a log scale except for day progress
class StockData:
    file_name_format = 'stock_data/{}.p'
    iex_database_persistence = 100 * day
    
    def __init__(self, symbol):
        self.symbol = symbol
        self.last_updated = datetime.date.today() \
                            - self.iex_database_persistence
        self.data = np.zeros((0, quote_size+1))

    def load_or_create(symbol):
        file_name = StockData.file_name_format.format(symbol)
        if isfile(file_name):
            stock_data = pickle.load(open(file_name, 'rb'))
            assert type(stock_data) is StockData
            return stock_data
        else:
            return StockData(symbol)

    def save(self):
        file_name = self.file_name_format.format(self.symbol)
        pickle.dump(self, open(file_name, 'wb'))

    def update(self):
        print('Updating {}'.format(self.symbol))
        today = datetime.date.today()
        while self.last_updated + day < today:
            date = self.last_updated + day
            self.last_updated = date
            print(date)
            chart = get_chart_matrix(self.symbol, date)
            self.data = np.concatenate((self.data, chart))

    def get_current_data(self):
        today = datetime.date.today()
        chart = get_chart_matrix(self.symbol, date)
        return np.concatenate((self.data, chart))

    def get_data(self):
        return self.data

import bs4 as bs

def get_sp500_tickers():
    resp = requests.get('http://en.wikipedia.org/wiki/List_of_S%26P_500_companies')
    soup = bs.BeautifulSoup(resp.text, 'lxml')
    table = soup.find('table', {'class': 'wikitable sortable'})
    tickers = []
    for row in table.findAll('tr')[1:]:
        ticker = row.findAll('td')[0].text
        tickers.append(ticker)
    return tickers

def update_stock_data(symbols):
    for symbol in symbols:
        stock_data = StockData.load_or_create(symbol)
        stock_data.update()
        stock_data.save()

def neural_network():
    import keras
    from keras.constraints import max_norm
    from keras.layers import Add, Conv1D, Dropout, Input, ReLU, Reshape
    from keras.losses import mean_squared_error
    from keras.models import Model
    from keras.optimizers import Adadelta
    
    inputs = Input(shape=(None, quote_size+1))
    current_layer = inputs
    skip_connections = [current_layer]
    dilation = 1
    for _ in range(5):
        for _ in range(2):
            current_layer = Conv1D(
                filters=8,
                kernel_size=3,
                dilation_rate=dilation,
                kernel_constraint=max_norm(),
                bias_constraint=max_norm())(current_layer)
            current_layer = ReLU(negative_slope=0.1)(current_layer)
            current_layer = Dropout(
                rate=0.2, noise_shape=(1, 1, 8))(current_layer)
            dilation *= 2
        residual = skip_connections[-1]
        current_shape = keras.backend.shape(current_layer)
        residual_shape = keras.backend.shape(residual)
        shape_difference = keras.layers.Subtract()(
            [residual_shape, current_shape])
        residual = keras.backend.slice(
            residual, shape_difference, current_shape)
        residual = Conv1D(filters=8, kernel_size=1)(residual)
        current_layer = Add()([residual, current_layer])
        skip_connections.append(current_layer)
    outputs = skip_connections[0]
    for layer in skip_connections[1:]:
        outputs = Add()([outputs, layer])
    outputs = ReLU(negative_slope=0.1)(outputs)
    outputs = Conv1D(filters=8, kernel_size=1)(outputs)
    outputs = ReLU(negative_slope=0.1)(outputs)
    outputs = Conv1D(filters=1, kernel_size=1)(outputs)
    outputs = Reshape((-1,))(outputs)
    model = Model(inputs=inputs, outputs=outputs)
    model.compile(loss=mean_squared_error,
                  optimizer=Adadelta(),
                  metrics=['accuracy'])
    return model

def softmax_choice(options, temperature):
    options /= temperature
    options -= np.amax(options)
    softmax = np.exp(options)
    softmax /= np.sum(softmax)
    return np.random.choice(options.size, p=softmax)

def generate_immediate_rewards(model, data, temperature):
    inputs = data[:, :-chart_delay_minutes, :]
    predictions = model.predict(inputs, batch_size=1, verbose=1)[0, :]
    data = data[0, :, :]
    num_time_steps = predictions.shape[0]
    assert data.shape[0] == num_time_steps + chart_delay_minutes
    rewards = np.empty(num_time_steps)
    ln_price_idx = quote_indices[market_average]
    bought = False
    can_buy_or_sell = True
    day_progress = 0
    for time_step in range(num_time_steps):
        if data[time_step, -1] < day_progress:
            can_buy_or_sell = True
        day_progress = data[time_step, -1]
        if bought:
            data_time_step = time_step + chart_delay_minutes
            rewards[time_step] = data[data_time_step, ln_price_idx] \
                                 - data[data_time_step-1, ln_price_idx]
        else:
            rewards[time_step] = 0
        if can_buy_or_sell:
            buy_sell_choice = softmax_choice(
                np.array([0, predictions[timestamp]]), temperature)
            assert type(buy_sell_choice) is int
            if bought and buy_sell_choice == 0:
                bought = False
                can_buy_or_sell = False
            elif not bought and buy_sell_choice == 1:
                bought = True
                can_buy_or_sell = False
    return rewards, predictions[-1]

q_discount_factor = 0.75 ** (1.0/390)

def q_values_from_immediate_rewards(rewards, last):
    num_time_steps = rewards.size
    q_values = np.empty(num_time_steps)
    q_values[-1] = last
    for time_step in range(num_time_steps-2, -1, -1):
        last *= q_discount_factor
        last += rewards[time_step]
        q_values[time_step] = last
    return q_values

def generate_training_data(model, symbol, temperature):
    data = StockData.load_or_create(symbol).get_data()[np.newaxis, :, :]
    rewards, last = generate_immediate_rewards(model, data, temperature)
    q_values = q_values_from_immediate_rewards(rewards, last)
    return data[:, :-chart_delay_minutes, :], q_values[np.newaxis, :]

from keras.models import load_model

model_file_path = 'model.h5'

def save_model(model):
    model.save(model_file_path)

def load_or_create_model():
    if isfile(model_file_path):
        return load_model(model_file_path)
    else:
        return neural_network()

training_temperature    = 0.001
testing_temperature     = 0.0001
predicting_temperature  = 0.0001

def train_on_symbols(symbols):
    model = load_or_create_model()
    for symbol in symbols:
        data, q_values = generate_training_data(
            model, symbol, training_temperature)
        model.fit(x=data, y=q_values, batch_size=1)
    save_model(model)

days_per_year = 365

def evaluate_on_symbols(symbols):
    model = load_or_create_model()
    total_ln_annual_returns = 0
    for symbol in symbols:
        data = StockData.load_or_create(symbol).get_data()[np.newaxis, :, :]
        rewards, _ = generate_immediate_rewards(
            model, data, testing_temperature)
        total_ln_annual_returns += np.sum(rewards) \
                                   * minutes_per_trading_day \
                                   * days_per_year \
                                   / rewards.size
    annual_returns = np.exp(total_ln_annual_returns / len(symbols))
    print('Average of {}% returns per year'.format(annual_returns - 1))

def should_i_buy(symbol):
    model = load_or_create_model()
    data = StockData.load_or_create(symbol).get_current_data()[np.newaxis, :, :]
    _, last = generate_immediate_rewards(model, data, predicting_temperature)
    return 0 < last

def test():
    for test_function in test_functions:
        test_function()
    print('All tests pass!')

def main():
    sp500_tickers = get_sp500_tickers()
    #update_stock_data(['tqqq', 'sqqq'] + sp500_tickers)
    train_on_symbols(sp500_tickers[100:])
    evaluate_on_symbols(sp500_tickers[:100])
    should_i_buy('tqqq')

if testing_mode:
    test()
else:
    main()
input('Press enter to exit...')
