def inputs(*args, **kwargs):
    def decorator(func):
        def wrapper():
            return func(*args, **kwargs)
        return wrapper
    return decorator

def processes_output(process):
    def decorator(func):
        def wrapper(*args, **kwargs):
            output = func(*args, **kwargs)
            process(output)
            return output
        return wrapper
    return decorator

def transforms_output(transform):
    def decorator(func):
        def wrapper(*args, **kwargs):
            output = func(*args, **kwargs)
            return transform(output)
        return wrapper
    return decorator

def compose_funcs(*funcs):
    func = funcs[0]
    for transform in funcs[1:]:
        decorator = transform_output(transform)
        func = decorator(func)

def compose_decorators(*decorators):
    def decorator(func):
        for decorator in decorators:
            func = decorator(func)
        return func
    return decorator

def process_args(args, arg_funcs):
    for arg, arg_func in zip(args, arg_funcs):
        yield arg_func(arg)

def process_kwargs(kwargs, kwarg_funcs):
    for key, kwarg in kwargs.items():
        if key in kwarg_funcs:
            yield key, kwarg_funcs[key](kwarg)

def exhaust(iterator):
    for _ in iterator:
        pass

def preprocesses_input(*arg_funcs, **kwarg_funcs):
    def decorator(func):
        def wrapper(*args, **kwargs):
            exhaust(process_args(arg_funcs, args))
            exhaust(process_kwargs(kwarg_funcs, kwargs))
            return func(*args, **kwargs)
        return wrapper
    return decorator

def postprocesses_input(*arg_funcs, **kwarg_funcs):
    def decorator(func):
        def wrapper(*args, **kwargs):
            output = func(*args, **kwargs)
            exhaust(process_args(arg_funcs, args))
            exhaust(process_kwargs(kwarg_funcs, kwargs))
            return output
        return wrapper
    return decorator

def transform_args(args, arg_funcs):
    for idx, transformed in enumerate(process_args(args, arg_funcs)):
        args[idx] = transformed

def transform_kwargs(kwargs, kwarg_funcs):
    for key, transformed in process_kwargs(kwargs, kwarg_funcs):
        kwargs[key] = transformed

def transforms_input(*arg_funcs, **kwarg_funcs):
    def decorator(func):
        def wrapper(*args, **kwargs):
            transform_args(arg_funcs, args)
            transform_kwargs(kwarg_funcs, kwargs)
            return func(*args, **kwargs)
        return wrapper
    return decorator

def returns_exception(exception_type):
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except exception_type as err:
                return err
        return wrapper
    return decorator
