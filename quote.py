import numpy as np
import schema
import test

@test.do(test.case(-1).should_return(False))
@test.do(test.case(0).should_return(False))
@test.do(test.case(1).should_return(True))
def _is_positive(num):
    return 0 < num

@test.do(test.table([
    (-1,        True),
    (-0.001,    True),
    (0,         False),
    (0.001,     False),
    (1,         False)]).arg_index(0).return_index(1))
def _is_negative(num):
    return num < 0

def _is_not_negative(num):
    return 0 <= num

def _is_not_positive(num):
    return num <= 0

minutes_per_trading_day = 390

_quote_keys = [
    'marketOpen',
    'marketClose',
    'marketHigh',
    'marketLow',
    'marketAverage',
    'marketVolume',
    'marketNumberOfTrades']

(open_idx,          _open), \
(close_idx,         _close), \
(high_idx,          _high), \
(low_idx,           _low), \
(average_idx,       _average), \
(volume_idx,        _volume), \
(num_trades_idx,    _num_trades), \
    = enumerate(_quote_keys)

quote_size = len(_quote_keys) + 1

_positive_float = schema.And(float, _is_positive)
_non_negative_int = schema.And(int, _is_not_negative)

_complete_quote = schema.Schema({
    _open:          _positive_float,
    _close:         _positive_float,
    _high:          _positive_float,
    _low:           _positive_float,
    _average:       _positive_float,
    _volume:        _non_negative_int,
    _num_trades:    _non_negative_int},
    ignore_extra_keys=True)

_incomplete_quote = schema.Schema({
    _high_price:        -1,
    _low_price:         -1,
    _average_price:     -1,
    _volume:            0,
    _number_of_trades:  0},
    ignore_extra_keys=True)

_quote_schema = schema.Or(_complete_quote, _incomplete_quote)
_chart_schema = schema.Schema([_quote_schema])

def _validate_quote_json(json):
    _quote_schema.validate(json)

def _validate_chart_json(json):
    _chart_schema.validate(json)

def _validate_quote(quote):
    if quote is None:
        return
    test.equal(quote.shape, (quote_size,))
    for positive_idx in [open_idx, close_idx, high_idx, low_idx, average_idx]:
        test.true(0 < chart[positive_idx])
    for non_negative_idx in [volume_idx, num_trades_idx, -1]:
        test.true(0 <= chart[non_negative_idx])

def _validate_chart(chart):
    test.equal(len(chart.shape), 2)
    test.true(chart.shape[0] <= minutes_per_trading_day)
    test.true((chart[:-1, -1] < chart[1:, -1]).all())
    test.true((chart[:, -1] <= np.arange(chart.shape[0])).all())
    for quote in chart:
        test.false(quote == None)
        _validate_quote(quote)

@test.input(_validate_quote)
def _is_complete(quote):
    return 0 < quote[high_price]

@test.input(lambda json, _: _validate_quote_json(json))
@test.output(_validate_quote)
def _quote_from_json(json, idx):
    if not _is_complete(json):
        return None
    quote = np.empty(quote_size)
    quote[-1] = idx
    for idx, key in enumerate(_keys):
        quote[idx] = json[key]
    return quote

@test.input(_validate_chart_json)
def _quotes_from_json(chart_json):
    for idx, quote_json in enumerate(chart_json):
        yield _quote_from_json(quote_json, idx)

@test.output(_validate_chart)
def _chart_from_quotes(quotes):
    return np.array([quote for quote in quotes if quote is not None])

@test.input(_validate_chart_json)
@test.output(_validate_chart)
def _chart_from_json(json):
    return _chart_from_quotes(_quotes_from_json(json))

def _chart_url(symbol, date):
    return 'https://api.iextrading.com/1.0/stock/{}/chart/date/{}'.format(
        symbol, date.strftime('%Y%m%d'))

@test.output(_validate_chart_json)
def _request_chart_json(symbol, date):
    url = chart_url(symbol, date)
    return requests.get(url).json()

@test.output(_validate_chart)
def request_chart(symbol, date):
    json = _request_chart_json(symbol, date)
    return _chart_from_json(json)
