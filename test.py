import functional as f

class Case:

    def __init__(self, *args, **kwargs):
        self._params = f.Params(*args, **kwargs)

    def returns(self, expected):
        return ReturnCase(self._params, expected)

    def raises(self, expected_type):
        return RaiseCase(self._params, expected_type)

class ReturnCase:

    def __init__(self, params, expected):
        self._params = params
        self._expected = expected

    def tests(self, func):
        
